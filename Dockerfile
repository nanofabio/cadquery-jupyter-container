FROM jupyter/datascience-notebook:lab-3.2.0
# FROM jupyter/datascience-notebook:python-3.7.6
# ^ currently pinning because some packages don't work with ptyhon 3.8
# Get the latest image tag at:
# https://hub.docker.com/r/jupyter/datascience-notebook/tags/
# Inspect the Dockerfile at:
# https://hub.docker.com/r/jupyter/datascience-notebook/dockerfile

# install additional package...
# bio packages compiled from source or downloaded as binaries will live in the /bio directory
USER root
ENV BIO_DIR=/bio
ENV PATH=$BIO_DIR/bin:$PATH

RUN mkdir -p $BIO_DIR
RUN mkdir -p $BIO_DIR/bin

# install additional packages for prereqs
RUN apt-get update
RUN apt-get install -yq --no-install-recommends \
	asciinema \
	bc \
	build-essential \
	chromium-browser \
	csh \
	curl \
	default-jdk \
	firefox \
	fuse3 \
	graphviz \
	hashdeep \
	htop \
	jq \
	less \
	libbz2-dev \
	libfuse2 \
	libgconf-2-4 \
	libgl1-mesa-glx \
	libgtk2.0-0 \
	liblzma-dev \
	libncurses5-dev \
	libssl-dev \
	libxrender1 \
	libxext6 \
	libxt6 \
	parallel \
	postgresql \
	postgresql-client \
	rename \
	rsync \
	screen \
	ssh \
	tmux \
	tree \
	vim \
	wget \
	xvfb \
	zlib1g-dev \
	zip \
    iputils-ping \
    iputils-tracepath \
    iproute2 

RUN curl https://rclone.org/install.sh | bash

USER $NB_UID
ENV BIO_DIR=/bio
ENV PATH=$BIO_DIR/bin:$PATH
ENV CONDA_ENVS=/opt/conda/envs

# Here I'm just installing directly to the base conda installation
RUN mamba install \
    -c anaconda \
    -c bioconda \
    -c bokeh \
    -c conda-forge \
    -c plotly \
    papermill \
    bash_kernel \
    biopython \
    bokeh \
    colorlover \
    dash \
    datashader \
    gitpython \
    hdbscan \
    htseq \
    humanize \
    plotly \
    plotly-orca \
    primer3-py \
    pytables \
    umap-learn \
    xarray \
    dask \
    distributed \
    netCDF4 \
    bottleneck \
    xmltodict \
    selenium \
    geckodriver \
    python-chromedriver-binary \
    jupyterlab-git \
    jupyter-server-proxy \
    dask-labextension \
    pyfuse3
    
# the pyviz package repo is much better at being up to date
RUN mamba install \
    -c pyviz \
    holoviews \
    hvplot \
    panel \
    pyviz_comms \
    markdown

# the following either can't be found in conda or are severely out of date
RUN pip install \
	exif \
	ratarmount \
	sh \
	jupyterthemes \
	opencv-python \
	imutils \
	graphviz \
	h5netcdf \
    rmfuse
# opencv is broken in conda right now (2020.12.15) might come back to life later
  
RUN jt -t grade3 -cellw 80% -ofs 11 -dfs 10 -fs 11 -tfs 11 -nfs 11 -mathfs 11

# RUN jupyter labextension install @pyviz/jupyterlab_pyviz

# install the CadQuery tools for Jupyterlab
RUN mamba install -c cadquery -c conda-forge cadquery=master
RUN pip install jupyter-cadquery==2.2.0
